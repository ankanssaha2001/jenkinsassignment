package com.wipro.scientificalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScientificalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScientificalculatorApplication.class, args);
	}

}
