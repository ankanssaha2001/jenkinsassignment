package com.wipro.scientificalculator;

import com.wipro.scientificalculator.TrigCalc;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    TrigCalc trigCalc = new TrigCalc();

    @Test
    public void testSinValueZero() {
        assertEquals(0.0, trigCalc.getSinValue(0), 0.001);
    }

    @Test
    public void testSinValueThirty() {
        assertEquals(0.5, trigCalc.getSinValue(30), 0.001);
    }

    @Test
    public void testSinValueNinety() {
        assertEquals(1.0, trigCalc.getSinValue(90), 0.001);
    }

    @Test
    public void testSinValueNegative() {
        assertEquals(-0.5, trigCalc.getSinValue(-30), 0.001); // Should pass now
    }

}
